import readlineSync from 'readline-sync';

let nvowels = (aletter) => {
    let avowels = ["a", "e", "i", "o", "u","A","E","I","O","U"]
    if(avowels.includes(aletter))
    return true;
}

let nString = readlineSync.question("Please enter a string: ");
//processing
let newstring="";
let nletters = nString.split(' ');
for(let n = 0; n < nletters.length; n++){
    let aletter = nletters[n].split('');
    let newword = "";
    for(let m = 0; m < aletter.length; m++){
        if(nvowels(aletter[m])){
            break;
        }
        newword = nletters.slice(m,nletters.length);//+nletters.slice(0,m-1)+"ay";
    }
    newstring = newstring + newword;
}
//output
console.log(`${newstring}`);